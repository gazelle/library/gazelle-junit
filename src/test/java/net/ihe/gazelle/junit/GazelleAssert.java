package net.ihe.gazelle.junit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertTrue;

/**
 * Created by ceoche on 20/07/15.
 */
public class GazelleAssert {

    /**
     * Test equality between two lists without verifying sorting order. Duplications are taken into account.
     *
     * @param result
     * @param expected
     */
    static public void assertListEqualsAnyOrder(List result, List expected) {
        List resultDuplicated;
        List expectedDuplicated;
        assertTrue(result.size() == expected.size());
        assertTrue(result.containsAll(expected) && expected.containsAll(result));

        /* if result = {e1, e1, e2} and expected = {e1, e2, e2},
         * lists are not the sames, but because they have same size and each list contains every elements, assert will
         * succeed.
         * So we must extract duplications in lists and perform again assertListEqualsAnyOrder() on those duplication
         * lists in a recursive way until there is no more duplications.
         */
        resultDuplicated = findDuplicates(result);
        expectedDuplicated = findDuplicates(expected);
        if (!resultDuplicated.isEmpty() && !expectedDuplicated.isEmpty()) {
            assertListEqualsAnyOrder(resultDuplicated, expectedDuplicated);
        }
    }

    /**
     * Find duplicated values in a list
     *
     * @param listContainingDuplicates list to explore
     *
     * @return the list of all duplicated objects in listContainingDuplicates. If element is contained 3 times in the
     * original list, it will appear 2 times.
     */
    static private List<Object> findDuplicates(List listContainingDuplicates) {

        final List<Object> listOfDuplicates = new ArrayList<Object>();
        final Set<Object> set1 = new HashSet();

        for (Object element : listContainingDuplicates) {
            if (!set1.add(element)) {
                listOfDuplicates.add(element);
            }
        }
        return listOfDuplicates;
    }
}
package net.ihe.gazelle.junit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extend this class to perform queries on a local Gazelle database, while testing.
 *
 * @author jlabbe
 * @version $Revision: 1.0 $
 */
public abstract class AbstractTestQueryJunit4 {

    /**
     * Field logger.
     */
    private static Logger logger = LoggerFactory.getLogger(AbstractTestQueryJunit4.class);

    /**
     * Field HIBERNATE_XML. (value is "HibernateConfiguration.HIBERNATE_XML")
     */
    public static final String HIBERNATE_XML = HibernateConfiguration.HIBERNATE_XML;

    /**
     * Method getDb. call it to setup the database used for testing
     *
     * @return String
     */
    protected String getDb() {
        return "gazelle-junit";
    }

    /**
     * Method getHbm2ddl. Used to control how hibernate manages database schema migrations possible values are: validate, update, create, create-drop.
     *
     * @return String
     */
    protected String getHbm2ddl() {
        return "create-drop";
    }

    /**
     * Method getServer. Specify the database server name
     *
     * @return String
     */
    protected String getServer() {
        return "localhost";
    }

    /**
     * Method showSql.
     *
     * @return boolean
     */
    protected boolean getShowSql() {
        return false;
    }

    /**
     * Method setUp. Call it in your Test setUp function
     *
     * @throws Exception
     */
    public void setUp() throws Exception {
        logger.debug("setup Hibernate JPA layer.");

        HibernateConfiguration.setConfigOnCurrentThread("jdbc:postgresql://" + getServer() + "/" + getDb(),
                getHbm2ddl(), getShowSql());
    }

    /**
     * Method tearDown. call it in your test tearDown function
     *
     * @throws Exception
     */
    public void tearDown() throws Exception {
        logger.debug("Shuting down Hibernate JPA layer.");
    }
}

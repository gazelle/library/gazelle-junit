package net.ihe.gazelle.junit;

import net.ihe.gazelle.hql.providers.detached.AbstractEntityManagerProvider;
import org.kohsuke.MetaInfServices;

@MetaInfServices(net.ihe.gazelle.hql.providers.EntityManagerProvider.class)
public class TestEntityManagerProvider extends AbstractEntityManagerProvider {

    @Override
    public Integer getWeight() {
        return 10;
    }

    @Override
    public String getHibernateConfigPath() {
        return AbstractTestQueryJunit4.HIBERNATE_XML;
    }
}

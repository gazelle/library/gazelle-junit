package net.ihe.gazelle.junit;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TransactionRequiredException;

public class DatabaseManager {
    private static final Logger LOG = LoggerFactory.getLogger(DatabaseManager.class);

    public static Object writeObject(Object obj) {
        EntityManager em = null;
        em = EntityManagerService.provideEntityManager();
        if (em != null) {
            try {
                if (!em.getTransaction().isActive()) {
                    em.getTransaction().begin();
                }
                obj = em.merge(obj);
                em.flush();
                em.getTransaction().commit();
            } catch (javax.validation.ConstraintViolationException ex) {
                // this EntityManager has been closed
                em.getTransaction().rollback();
                LOG.warn(ex.getMessage());
            } catch (IllegalStateException ex) {
                // this EntityManager has been closed
                em.getTransaction().rollback();
                LOG.warn(ex.getMessage());
            } catch (TransactionRequiredException ex) {
                // there is no transaction
                em.getTransaction().rollback();
                LOG.warn(ex.getMessage());
            } catch (PersistenceException ex) {
                em.getTransaction().rollback();
                LOG.warn(ex.getMessage());
                // the flush fails
            }
        }
        return obj;
    }

    public static void removeObject(Object obj) {
        EntityManager em = EntityManagerService.provideEntityManager();
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        em.remove(obj);
        em.flush();
        em.getTransaction().commit();
    }

    public static void update(Object... to_update) {
        EntityManager em = EntityManagerService.provideEntityManager();
        for (Object object : to_update) {
            em.refresh(object);
        }
    }
}
package net.ihe.gazelle.junit;

import org.reflections.Reflections;

import javax.persistence.Entity;
import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HibernateConfiguration {

    public static final String HIBERNATE_XML = "exchange.hibernate.xml";

    private static Reflections REFLECTIONS = null;

    private HibernateConfiguration() {
        super();
    }

    public static void setConfigOnCurrentThread(String jdbcUrl, String hbm2ddl, boolean showsql) {
        final String config = createConfig(jdbcUrl, hbm2ddl, showsql);

        final ClassLoader currentContextClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(new ClassLoader() {
            @Override
            public InputStream getResourceAsStream(String name) {
                if (HIBERNATE_XML.equals(name) || "META-INF/hibernate.cfg.xml".equals(name)) {
                    return new ByteArrayInputStream(config.getBytes());
                } else {
                    return currentContextClassLoader.getResourceAsStream(name);
                }
            }
        });
    }

    private static String createConfig(String jdbcUrl, String hbm2ddl, boolean showsql) {
        try {
            return createConfigWithIOException(jdbcUrl, hbm2ddl, showsql);
        } catch (IOException e) {
            throw new RuntimeException("Failed to generate config", e);
        }
    }

    private static String createConfigWithIOException(String jdbcUrl, String hbm2ddl, boolean showsql) throws IOException {
        StringWriter sw = new StringWriter();
        BufferedWriter bw = new BufferedWriter(sw);

        bw.write("<?xml version='1.0' encoding='utf-8'?>");
        bw.newLine();
        bw.write("<!DOCTYPE hibernate-configuration PUBLIC \"-//Hibernate/Hibernate Configuration DTD//EN\" \"http://hibernate.sourceforge.net/hibernate-configuration-3.0.dtd\">");
        bw.newLine();
        bw.write("<hibernate-configuration>");
        bw.newLine();
        bw.write("	<session-factory>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.connection.driver_class\">org.postgresql.Driver</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.connection.url\">" + jdbcUrl + "</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.connection.username\">gazelle</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.connection.password\">gazelle</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.dialect\">org.hibernate.dialect.PostgreSQLDialect</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.hbm2ddl.auto\">" + hbm2ddl + "</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.show_sql\">" + showsql + "</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.format_sql\">true</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.connection.provider_class\">org.hibernate.connection.C3P0ConnectionProvider</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.c3p0.min_size\">0</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.c3p0.max_size\">3</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.c3p0.acquire_increment\">1</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.c3p0.idle_test_period\">3000</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.c3p0.max_statements\">50</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.c3p0.timeout\">18000</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.cache.use_query_cache\">false</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.cache.use_second_level_cache\">true</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.cache.use_query_cache\">true</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.cache.region_prefix\"></property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.cache.provider_class\">org.hibernate.service.jdbc.connections.internal.C3P0ConnectionProvider</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.cache.provider_configuration_file_resource_path\">/ehcache.xml</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.listeners.envers.autoRegister\">false</property>");
        bw.newLine();
        bw.write("		<property name=\"hibernate.cache.region.factory_class\">org.hibernate.cache.ehcache.EhCacheRegionFactory</property>");
        bw.newLine();

        List<String> entities = retrieveEntities();
        for (String entity : entities) {
            if (entity != null) {
                bw.write("		<mapping class=\"" + entity + "\" />");
                bw.newLine();
            }
        }

        bw.write("		</session-factory>");
        bw.newLine();
        bw.write("</hibernate-configuration>");
        bw.newLine();

        bw.close();

        return sw.toString();
    }

    private static Reflections getReflections() {
        if (REFLECTIONS == null) {
            synchronized (HibernateConfiguration.class) {
                if (REFLECTIONS == null) {
                    REFLECTIONS = new Reflections("net");
                }
            }
        }
        return REFLECTIONS;
    }

    private static List<String> retrieveEntities() {
        Set<Class<?>> entities = getReflections().getTypesAnnotatedWith(Entity.class);
        Set<String> classes = new HashSet<String>();
        for (Class<?> class1 : entities) {
            classes.add(class1.getCanonicalName());
        }
        return new ArrayList<String>(classes);
    }
}

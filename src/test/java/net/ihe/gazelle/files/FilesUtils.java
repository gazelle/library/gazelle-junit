package net.ihe.gazelle.files;

import org.apache.commons.io.FileUtils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

/**
 * Created by epoiseau on 31/05/2016.
 */
public class FilesUtils {

    public static File loadFile(String fileName) {
        URL file_url = FilesUtils.class.getResource(fileName);
        File file = new File(file_url.getFile());
        return file;
    }

    public static String getResourcePath(String fileName) {
        URL file_url = FilesUtils.class.getResource(fileName);
        return file_url.getFile();
    }

    public static String loadStringsFromFile(String fileName) {
        URL file_url = FilesUtils.class.getResource(fileName);
        // if resource not found return null
        if (file_url == null){
            return null ;
        }

        File file = new File(file_url.getFile());
        String fileContent = null;

        try {
            fileContent = FileUtils.readFileToString(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileContent;
    }

    public String fileToString(File file) {
        String result = null;
        DataInputStream in = null;

        try {
            byte[] buffer = new byte[(int) file.length()];
            in = new DataInputStream(new FileInputStream(file));
            in.readFully(buffer);
            result = new String(buffer);
        } catch (IOException e) {
            throw new RuntimeException("IO problem in fileToString", e);
        } finally {
            try {
                in.close();
            } catch (IOException e) { /* ignore it */
            }
        }
        return result;
    }
}

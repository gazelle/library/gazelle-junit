package test;

import static org.junit.Assert.assertTrue;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class testConnection extends AbstractTestQueryJunit4 {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		if (EntityManagerService.provideEntityManager().getTransaction().isActive()) {
			EntityManagerService.provideEntityManager().getTransaction().commit();
		}
		super.tearDown();
	}

	@Override
	protected String getHbm2ddl() {
		return "auto";
	}

	@Override
	protected String getDb() {
		return "evs-client-junit";
	}

	@Test
	public void connection() {
		EntityManagerService.provideEntityManager().getTransaction().begin();
		assertTrue(true);
	}
}
